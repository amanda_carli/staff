package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContaEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue( generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;

    @OneToMany( mappedBy = "tipoConta" )
    private List<ContaEntity> contas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}
