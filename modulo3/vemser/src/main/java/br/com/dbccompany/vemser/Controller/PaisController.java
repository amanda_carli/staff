package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.DTO.GeralDTO;
import br.com.dbccompany.vemser.DTO.PaisDTO;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.PaisService;
import br.com.dbccompany.vemser.VemserApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pais")
public class PaisController {

    private Logger logger = LoggerFactory.getLogger(VemserApplication.class);

    @Autowired
    PaisService service;

    //localhost:8080/api/pais/todos
    //Header && Body
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PaisEntity> todosPais() {
        logger.info("Começou a buscar os paises!");
        return service.todos();
    }

    //Request ->  <- Response
    //200, 202,  303, 404
    @PostMapping( value = "/novo" )
    @ResponseBody
    public PaisEntity salvar(@RequestBody PaisEntity pais){
        return service.salvar(pais);
    }

    //localhost:8080/api/pais/ver/1
    //localhost:8080/api/pais/ver/2
    //localhost:8080/api/pais/ver/3
    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PaisEntity paisEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PaisEntity editarPais(@PathVariable Integer id, @RequestBody PaisEntity pais){
        return service.editar(pais, id);
    }

}
