package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.DTO.PaisDTO;
import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.EnderecoService;
import br.com.dbccompany.vemser.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/endereco" )
public class EnderecoController {

    @Autowired
    EnderecoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EnderecoEntity> todosPais() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EnderecoEntity salvar(@RequestBody EnderecoEntity endereco){
        return service.salvar(endereco);
    }

}
