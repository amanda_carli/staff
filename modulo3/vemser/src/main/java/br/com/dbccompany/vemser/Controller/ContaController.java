package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Service.ContaService;
import br.com.dbccompany.vemser.Service.EstadoService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/conta" )
public class ContaController {

    @Autowired
    ContaService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContaEntity> todosEstados() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContaEntity salvar(@RequestBody @NotNull ContaEntity estado){
        return service.salvar(estado);
    }

    @GetMapping( value = "/ver/{id}/{idTipoConta}")
    @ResponseBody
    public ContaEntity estadoEspecifico(@PathVariable Integer id, @PathVariable Integer idTipoConta){
        ContaEntityId newId = new ContaEntityId(id, idTipoConta);
        return service.porId(newId);
    }

    @PutMapping( value = "/editar/{id}/{idTipoConta}" )
    @ResponseBody
    public ContaEntity editarEstado(@PathVariable Integer id,
                                    @PathVariable Integer idTipoConta,
                                    @RequestBody ContaEntity estado){
        return service.editar(estado, id, idTipoConta);
    }
}
