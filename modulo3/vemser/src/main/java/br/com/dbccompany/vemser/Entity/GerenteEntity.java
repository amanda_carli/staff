package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/* Herança:
        - Tabelão ( campo tipo referencia tabela filha ) -> PF ou PJ | Single
        - Tabela mãe ( Coloca FK dentro das tabelas filhas ) -> Usuario (Mãe e utilizavel) + tabelas filhas | JOINED
        - Cria somente as tabelas filhas com todos os campos. -> Usuario (abstrata) + 2 tabelas (Cliente e Gerente) | Table per class ou super
*/

@Entity
@PrimaryKeyJoinColumn( name = "id")
public class GerenteEntity extends UsuarioEntity implements Serializable {

    private int codigo;

    @Enumerated( EnumType.STRING )
    private TipoGerenteEnum tipoGerente;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn( name = "id_gerente") },
            inverseJoinColumns = { @JoinColumn( name = "id_conta" ), @JoinColumn( name = "id_tipo_conta" ) }
    )
    private List<ContaEntity> contas;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public TipoGerenteEnum getTipoGerente() {
        return tipoGerente;
    }

    public void setTipoGerente(TipoGerenteEnum tipoGerente) {
        this.tipoGerente = tipoGerente;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}
