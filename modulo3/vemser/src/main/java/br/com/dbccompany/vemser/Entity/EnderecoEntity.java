package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

//new Agencia
//agencia.setCodigo(0001)
//agencia.setNome("agenciaWeb")

//new Endereco
//endereco.logradouro( "rua teste,001" )

//Unidirecional = Agencia ( 0001, "agenciaWeb",  endereco ) | Endereco ( "rua teste,001", agencia -> Não vai persistir )
//INSERT INTO AGENCIA VALUES (VALORES DA AGENCIA)
//Bidirecional = Agencia ( 0001, "agenciaWeb",  endereco ) | Endereco ( "rua teste,001", agencia )
//INSERT INTO AGENCIA VALUES (VALORES DA AGENCIA) && INSERT INTO ENDERECO VALUES (VALORES DA ENDERECO)
//&& VINCULA OS IDS

@Entity
public class EnderecoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
    @GeneratedValue( generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String logradouro;
    private int numero;
    private String complemento;
    @Column( length = 8, columnDefinition = "CHAR")
    private String cep;
    private String bairro;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CIDADE" )
    private CidadeEntity cidade;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_AGENCIA" )
    //@OneToOne( mappedBy = "endereco", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
    private AgenciaEntity agencia;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_USUARIO" )
    private UsuarioEntity usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public CidadeEntity getCidade() {
        return cidade;
    }

    public void setCidade(CidadeEntity cidade) {
        this.cidade = cidade;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }
}
