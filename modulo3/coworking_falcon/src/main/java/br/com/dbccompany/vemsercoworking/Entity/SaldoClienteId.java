package br.com.dbccompany.vemsercoworking.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {
    @Column(name = "ID_CLIENTE")
    private int idCliente;

    @Column(name = "ID_ESPACO")
    private int idEspaco;

    public SaldoClienteId() {}



    public SaldoClienteId(int idCliente, int idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(int idEspaco) {
        this.idEspaco = idEspaco;
    }
}
