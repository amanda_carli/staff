package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.vemsercoworking.Util.ConvertePrazoParaDiasUtil;

public class EspacoPacoteDTO {

    private Integer id;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private double prazo;
    private EspacoEntity espaco;
    private PacoteEntity pacote;

    public EspacoPacoteDTO() {}

    public EspacoPacoteDTO(EspacoPacoteEntity espacoPacote) {
        this.id = espacoPacote.getId();
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
        this.espaco = espacoPacote.getEspaco();
        this.pacote = espacoPacote.getPacote();
    }

    public EspacoPacoteEntity convert() {
        this.prazo = ConvertePrazoParaDiasUtil.converteParaDias(quantidade, tipoContratacao);
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setTipoContratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        espacoPacote.setEspaco(this.espaco);
        espacoPacote.setPacote(this.pacote);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrazo() {
        return prazo;
    }

    public void setPrazo(double prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }
}
