package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.UsuarioDTO;
import br.com.dbccompany.vemsercoworking.Entity.UsuarioEntity;
import br.com.dbccompany.vemsercoworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioDTO> todosUsuario() {
        return service.retornarListaUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public UsuarioDTO salvar(@RequestBody UsuarioDTO usuarioDTO){
        UsuarioEntity usuarioEntity = usuarioDTO.convert();
        UsuarioDTO newDto = new UsuarioDTO(service.salvar(usuarioEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public UsuarioDTO usuarioEspecifico(@PathVariable Integer id) {
        UsuarioDTO usuarioDTO = new UsuarioDTO(service.porId(id));
        return usuarioDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioDTO editarUsuario(@PathVariable Integer id, @RequestBody UsuarioDTO usuarioDTO) {
        UsuarioEntity usuario = usuarioDTO.convert();
        UsuarioDTO newDTO = new UsuarioDTO(service.editar(usuario, id));
        return newDTO;
    }

    @PostMapping( value = "/login" )
    @ResponseBody
    public boolean fazerLogin(@RequestBody UsuarioDTO login) {
        return service.login(login.getLogin(), login.getSenha());
    }
}
