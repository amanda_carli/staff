package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;

import java.util.List;

public class ClientePacoteDTO {

    private Integer id;
    private ClienteEntity cliente;
    private PacoteEntity pacote;
    private int quantidade;
    private List<PagamentoEntity> pagamentos;

    public ClientePacoteDTO() {}

    public ClientePacoteDTO(ClientePacoteEntity clientePacote) {
        this.id = clientePacote.getId();
        this.cliente = clientePacote.getCliente();
        this.pacote = clientePacote.getPacote();
        this.quantidade = clientePacote.getQuantidade();
        this.pagamentos = clientePacote.getPagamentos();
    }

    public ClientePacoteEntity convert() {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        if(this.cliente != null || this.pacote != null){
            clientePacote.setId(this.id);
            clientePacote.setCliente(this.cliente);
            clientePacote.setPacote(this.pacote);
            clientePacote.setQuantidade(this.quantidade);
            clientePacote.setPagamentos(this.pagamentos);
        }
        return clientePacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
