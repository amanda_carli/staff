package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Repository.EspacoRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacoService extends ServiceAbstract<EspacoRepository, EspacoEntity, Integer> {
}
