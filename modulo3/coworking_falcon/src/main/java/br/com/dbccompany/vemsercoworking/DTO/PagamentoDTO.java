package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoPagamentoEnum;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class PagamentoDTO {

    private Integer id;
    private ClientePacoteEntity clientePacote;
    private ContratacaoEntity contratacao;
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoDTO() {}

    public PagamentoDTO(PagamentoEntity pagamento) {
        this.id = pagamento.getId();
        this.clientePacote = pagamento.getClientePacote();
        this.contratacao = pagamento.getContratacao();
        this.tipoPagamento = pagamento.getTipoPagamento();
    }

    public PagamentoEntity convert() {
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setId(this.id);
        pagamento.setClientePacote(this.clientePacote);
        pagamento.setContratacao(this.contratacao);
        pagamento.setTipoPagamento(this.tipoPagamento);
        return pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
