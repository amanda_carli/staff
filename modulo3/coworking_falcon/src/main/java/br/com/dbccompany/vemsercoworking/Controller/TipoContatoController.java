package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.TipoContatoDTO;
import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import br.com.dbccompany.vemsercoworking.Service.TipoContatoService;
import br.com.dbccompany.vemsercoworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoDTO> todosTipoContato() {
        List<TipoContatoDTO> listaDTO = new ArrayList<>();
        for (TipoContatoEntity tipoContato : service.todos()) {
            listaDTO.add(new TipoContatoDTO(tipoContato));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContatoDTO salvar(@RequestBody TipoContatoDTO tipoContatoDTO){
        TipoContatoEntity tipoContatoEntity = tipoContatoDTO.convert();
        TipoContatoDTO newDto = new TipoContatoDTO(service.salvar(tipoContatoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public TipoContatoDTO tipoContatoEspecifico(@PathVariable Integer id) {
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.porId(id));
        return tipoContatoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContatoDTO editarTipoContato(@PathVariable Integer id, @RequestBody TipoContatoDTO tipoContatoDTO) {
        TipoContatoEntity tipoContato = tipoContatoDTO.convert();
        TipoContatoDTO newDTO = new TipoContatoDTO(service.editar(tipoContato, id));
        return newDTO;
    }
}
