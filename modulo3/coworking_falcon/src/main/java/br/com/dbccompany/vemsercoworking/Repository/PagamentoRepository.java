package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {

    List<PagamentoEntity> findAllByTipoPagamento( TipoPagamentoEnum tipo );
}
