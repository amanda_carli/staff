package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.PacoteDTO;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

    @Autowired
    PacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PacoteDTO> todosPacote() {
        List<PacoteDTO> listaDTO = new ArrayList<>();
        for (PacoteEntity pacote : service.todos()) {
            listaDTO.add(new PacoteDTO(pacote));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PacoteDTO salvar(@RequestBody PacoteDTO pacoteDTO) {
        PacoteEntity pacoteEntity = pacoteDTO.convert();
        PacoteDTO newDto = new PacoteDTO(service.salvar(pacoteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PacoteDTO pacoteEspecifico(@PathVariable Integer id) {
        PacoteDTO pacoteDTO = new PacoteDTO(service.porId(id));
        return pacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PacoteDTO editarPacote(@PathVariable Integer id, @RequestBody PacoteDTO pacoteDTO) {
        PacoteEntity pacote = pacoteDTO.convert();
        PacoteDTO newDTO = new PacoteDTO(service.editar(pacote, id));
        return newDTO;
    }
}
