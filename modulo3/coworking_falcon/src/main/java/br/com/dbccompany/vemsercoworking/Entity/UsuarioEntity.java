package br.com.dbccompany.vemsercoworking.Entity;

import br.com.dbccompany.vemsercoworking.Util.CriptografiaMD5Util;
import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class UsuarioEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    @Size(min = 6)
    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(@NotNull String senha) {
        for (int i = 0; i < senha.length(); i++) {
            boolean letraOuNumero = Character.isDigit(senha.charAt(i)) || Character.isLetter(senha.charAt(i));
            if (letraOuNumero) {
                continue;
            } else {
                return;
            }
        }
        this.senha = CriptografiaMD5Util.criptografaSenha(senha);
    }
}
