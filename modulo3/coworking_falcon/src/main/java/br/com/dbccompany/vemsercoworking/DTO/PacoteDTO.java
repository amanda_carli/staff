package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Util.TratarValorUtil;

import java.util.List;

public class PacoteDTO {

    private Integer id;
    private List<EspacoPacoteEntity> espacosPacotes;
    private String valor;
    private List<ClientePacoteEntity> clientesPacotes;

    public PacoteDTO() {}

    public PacoteDTO(PacoteEntity pacote) {
        this.id = pacote.getId();
        this.espacosPacotes = pacote.getEspacosPacotes();
        this.valor = TratarValorUtil.trataValorSaida(pacote.getValor());
        this.clientesPacotes = pacote.getClientesPacotes();
    }

    public PacoteEntity convert() {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setId(this.id);
        pacote.setEspacosPacotes(this.espacosPacotes);
        pacote.setValor(TratarValorUtil.trataValorEntrada(this.valor));
        pacote.setClientesPacotes(this.clientesPacotes);
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacoteEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
