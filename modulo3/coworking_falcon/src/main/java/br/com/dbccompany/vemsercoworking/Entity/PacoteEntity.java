package br.com.dbccompany.vemsercoworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToMany(mappedBy = "pacote")
    private List<EspacoPacoteEntity> espacosPacotes;

    private double valor;

    @Column( nullable = false )
    private String qualquerCampo;

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacoteEntity> clientesPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientePacoteEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacoteEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
