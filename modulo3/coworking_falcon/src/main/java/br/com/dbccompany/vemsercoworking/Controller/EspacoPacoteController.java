package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/espacoPacote")
public class EspacoPacoteController {

    @Autowired
    EspacoPacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoPacoteDTO> todosEspacoPacote() {
        List<EspacoPacoteDTO> listaDTO = new ArrayList<>();
        for (EspacoPacoteEntity espacoPacote : service.todos()) {
            listaDTO.add(new EspacoPacoteDTO(espacoPacote));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoPacoteDTO salvar(@RequestBody EspacoPacoteDTO espacoPacoteDTO){
        EspacoPacoteEntity espacoPacoteEntity = espacoPacoteDTO.convert();
        EspacoPacoteDTO newDto = new EspacoPacoteDTO(service.salvar(espacoPacoteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public EspacoPacoteDTO espacoPacoteEspecifico(@PathVariable Integer id) {
        EspacoPacoteDTO espacoPacoteDTO = new EspacoPacoteDTO(service.porId(id));
        return espacoPacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoPacoteDTO editarEspacoPacote(@PathVariable Integer id, @RequestBody EspacoPacoteDTO espacoPacoteDTO) {
        EspacoPacoteEntity espacoPacote = espacoPacoteDTO.convert();
        EspacoPacoteDTO newDTO = new EspacoPacoteDTO(service.editar(espacoPacote, id));
        return newDTO;
    }
}
