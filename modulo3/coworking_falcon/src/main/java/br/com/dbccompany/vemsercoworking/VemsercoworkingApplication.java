package br.com.dbccompany.vemsercoworking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VemsercoworkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(VemsercoworkingApplication.class, args);
	}

}
