package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Service.ClientePacoteService;
import br.com.dbccompany.vemsercoworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/clientePacote")
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientePacoteDTO> todosClientePacote() {
        List<ClientePacoteDTO> listaDTO = new ArrayList<>();
        for (ClientePacoteEntity cliente : service.todos()) {
            listaDTO.add(new ClientePacoteDTO(cliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientePacoteDTO salvar(@RequestBody ClientePacoteDTO clientePacoteDTO){
        ClientePacoteEntity clientePacoteEntity = clientePacoteDTO.convert();
        ClientePacoteDTO newDto = new ClientePacoteDTO(service.salvar(clientePacoteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClientePacoteDTO clientePacoteEspecifico(@PathVariable Integer id) {
        ClientePacoteDTO clientePacoteDTO = new ClientePacoteDTO(service.porId(id));
        return clientePacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientePacoteDTO editarPacoteCliente(@PathVariable Integer id, @RequestBody ClientePacoteDTO clientePacoteDTO) {
        ClientePacoteEntity clientePacote = clientePacoteDTO.convert();
        ClientePacoteDTO newDTO = new ClientePacoteDTO(service.editar(clientePacote, id));
        return newDTO;
    }
}
