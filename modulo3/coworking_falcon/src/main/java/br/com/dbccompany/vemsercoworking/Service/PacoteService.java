package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Repository.PacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class PacoteService extends ServiceAbstract<PacoteRepository, PacoteEntity, Integer> {
}
