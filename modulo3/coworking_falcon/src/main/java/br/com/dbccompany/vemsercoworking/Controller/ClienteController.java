package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.ClienteDTO;
import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
    @Autowired
    ClienteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClienteDTO> todosCliente() {
        List<ClienteDTO> listaDTO = new ArrayList<>();
        for (ClienteEntity cliente : service.todos()) {
            listaDTO.add(new ClienteDTO(cliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteDTO salvar(@RequestBody ClienteDTO clienteDTO) {
        ClienteEntity clienteEntity = clienteDTO.convert();
        ClienteDTO newDto = new ClienteDTO(service.salvar(clienteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClienteDTO clienteEspecifico(@PathVariable Integer id) {
        ClienteDTO clienteDTO = new ClienteDTO(service.porId(id));
        return clienteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteDTO editarCliente(@PathVariable Integer id, @RequestBody ClienteDTO clienteDTO) {
        ClienteEntity cliente = clienteDTO.convert();
        ClienteDTO newDTO = new ClienteDTO(service.editar(cliente, id));
        return newDTO;
    }
}
