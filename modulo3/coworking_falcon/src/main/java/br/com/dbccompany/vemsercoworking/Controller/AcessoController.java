package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.AcessoDTO;
import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import br.com.dbccompany.vemsercoworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    AcessoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<AcessoDTO> todosAcesso() {
        List<AcessoDTO> listaDTO = new ArrayList<>();
        for (AcessoEntity acesso : service.todos()) {
            listaDTO.add(new AcessoDTO(acesso));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public AcessoDTO salvar(@RequestBody AcessoDTO acessoDTO){
        return service.salvarEntrada(acessoDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public AcessoDTO acessoEspecifico(@PathVariable Integer id) {
        AcessoDTO acessoDTO = new AcessoDTO(service.porId(id));
        return acessoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AcessoDTO editarAcesso(@PathVariable Integer id, @RequestBody AcessoDTO acessoDTO) {
        AcessoEntity acesso = acessoDTO.convert();
        AcessoDTO newDTO = new AcessoDTO(service.editar(acesso, id));
        return newDTO;
    }
}
