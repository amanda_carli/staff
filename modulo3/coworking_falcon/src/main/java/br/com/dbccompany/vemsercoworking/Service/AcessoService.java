package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.DTO.AcessoDTO;
import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteId;
import br.com.dbccompany.vemsercoworking.Repository.AcessoRepository;
import br.com.dbccompany.vemsercoworking.Repository.EspacoRepository;
import br.com.dbccompany.vemsercoworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;

@Service
public class AcessoService extends ServiceAbstract<AcessoRepository, AcessoEntity, Integer> {

    @Autowired
    SaldoClienteRepository saldoRepository;

    public AcessoDTO salvarEntrada( AcessoDTO acesso ){
        if( acesso.isEntrada() ) {
            SaldoClienteId id = acesso.getSaldoCliente().getId();
            SaldoClienteEntity saldo = saldoRepository.findById(id).get();
            //saldo.getVencimento().compareTo(new LocalDate());
            if(saldo.getQuantidade() <= 0) {
                return null;
            }
        }
        AcessoEntity acessoEntity = acesso.convert();
        AcessoDTO newDto = new AcessoDTO(super.salvar(acessoEntity));
        return newDto;
    }

}
