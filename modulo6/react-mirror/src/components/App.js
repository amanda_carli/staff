import React, { Component } from 'react';
import './App.scss';

import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from './episodioUi';
import BotaoUi from './botaoUi';
import MensagemFlash from './mensagemFlash';
import MeuInputNumero from './meuInputNumero';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false,
      deveExibirErro: false
    };
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios;
    this.setState((state) => {
      return { ...state, episodio }
    })
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.marcarComoAssistido();
    this.setState( state => {
      return { ...state, episodio }
    })
  }

  registrarNota( { nota, erro } ) {
    this.setState( state => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }

    const { episodio } = this.state;
    let corMensagem, mensagem;
    
    if (episodio.validarNota( nota )) {
      episodio.avaliar( nota );
      corMensagem = 'verde';
      mensagem = 'Registramos sua nota!';
    }else{
      corMensagem = 'vermelho';
      mensagem = 'Informar uma nota válida (entre 1 e 5)';
    }

    this.exibirMensagem( { corMensagem, mensagem } );
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState( state => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState( state => {
      return { 
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  render(){
    const { episodio, deveExibirMensagem, mensagem, corMensagem, deveExibirErro } = this.state;

    return ( 
      <div className="App">
        <MensagemFlash
              atualizarMensagem={ this.atualizarMensagem }
              mensagem={ mensagem }
              cor={ corMensagem }
              exibir={ deveExibirMensagem }
              segundos="5" />
        <header className="App-header">
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <BotaoUi classe="verde" nome="Próximo" metodo={ this.sortear.bind( this ) } />
            <BotaoUi classe="azul" nome="Já Assisti!" metodo={ this.marcarComoAssistido } />
          </div>
          <span>Nota: { episodio.nota }</span>
          <div className="descricao-nota">
            <MeuInputNumero
                  placeholder="Nota de 1 a 5"
                  mensagem="Qual a sua nota para esse episódio?"
                  obrigatorio={ true }
                  visivel={ episodio.assistido || false }
                  deveExibirErro={ deveExibirErro }
                  atualizarValor={ this.registrarNota.bind( this ) } />
          </div>
        </header>
      </div>
    )
  };
}