import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MeuInputNumero extends Component {

  perdeFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props;
    const nota = evt.target.value;
    const erro = obrigatorio && !nota;
    atualizarValor( { nota, erro } );
  }
  
  mostrarNaTela() {
    /* const { visivel } = this.props;

    return(
      this.pro
    ) */

  }

  render(){
    const { placeholder, visivel, mensagem, deveExibirErro } = this.props;

    return visivel ? (
      <React.Fragment>
        { mensagem && <span>{ mensagem }</span> }
        <input type="number" className={ deveExibirErro ? 'erro' : '' } placeholder={ placeholder } onBlur={ this.perdeFoco } />
        { deveExibirErro && <span className="mensagem-erro" >* Obrigatório</span> }
      </React.Fragment>
    ) : null;
  }
}

MeuInputNumero.propTypes = {
  visivel: PropTypes.bool.isRequired,
  deveExibirErro: PropTypes.bool.isRequired,
  placeholder: PropTypes.string,
  mensagem: PropTypes.string,
  obrigatorio: PropTypes.bool
}

MeuInputNumero.defaultProps = {
  obrigatorio: false
}