let circulo = {
    raio: 3,
    tipoCalculo: "A"
}

function calcularCirculo({ raio, tipoCalculo:tipo }) {
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio);
}

console.log(calcularCirculo(circulo));

/** ---------------------FIM QUESTÃO 1-------------------------- */

function naoBissexto(ano) {
    return (ano %400 == 0) || (ano %4 == 0 && ano %100 != 0) ? false : true;
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

/** ---------------------FIM QUESTÃO 2-------------------------- */

function somarPares(numeros) {
    let resultado = 0;
    for (let i = 0; i < numeros.length; i++) {
        if( i %2 == 0){
            resultado += numeros[i];
        }
    }
    return resultado;
}

console.log(somarPares([1, 56, 4.34, 6, -2]));

/** ---------------------FIM QUESTÃO 3-------------------------- */

const aula = {
    turma: 2020,
    qtdAulunos: 12,
    qtdAulasNoModulo(modulo) {
        switch (modulo) {
            case 1:
                console.log(10);
                break;
            case 2:
                console.log(5);
                break;
            default:
                break;
        }
    }
}

aula.qtdAulasNoModulo(1);

/** ---------------------FIM EXEMPLO-------------------------- */

/*function adicionar(op1){
    return function(op2) {
        return op1 + op2;
    }
}*/

let adicionar = op1 => op2 => op1 + op2;

console.log(adicionar(3)(4));

/** ---------------------FIM QUESTÃO 4-------------------------- */

/* let is_divisivel = (divisor, numero) => !( numero % divisor );
const divisor = 2;
console.log(is_divisivel(divisor, 5));
console.log(is_divisivel(divisor, 8));
console.log(is_divisivel(divisor, 16));
console.log(is_divisivel(divisor, 9)); */

const divisivelPor = divisor => numero => !( numero % divisor );
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(5));
console.log(is_divisivel(8));
console.log(is_divisivel(16));
console.log(is_divisivel(9));

//console.log(somar(8));

/** ---------------------FIM EXEMPLO-------------------------- */