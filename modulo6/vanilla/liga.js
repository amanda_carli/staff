class Partidas {
    constructor(mandante, visitante, placar) {
        this._mandante = mandante;
        this._visitante = visitante;
        this._placar = placar;
    }

    get mandante(){
        return this._mandante;
    }

    get visitante() {
        return this._visitante;
    }

    get placar() {
        return this._placar;
    }
}

/** ---------------------Jogadores------------------- */

class Jogador {
    constructor(nome, numero) {
        this._nome = nome;
        this._numero = numero;
        this._capitao = false;
    }

    get nome(){
        return this._nome;
    }

    get numero() {
        return this._numero;
    }

    virarCapitao = () => {
        this._capitao = true;
    }
}

/** --------------------Time------------------------- */

class Time {
    constructor(nome, tipoEsporte, ligaQueJoga) {
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = "ativo";
        this._ligaQueJoga = ligaQueJoga;
        this._jogadores = [];
        this._partidas = [];
    }

    adicionarJogadores = ( jogador ) => {
        this._jogadores.push(jogador);
    }

    buscarJogadoresPorNome = ( nomeJogador ) => {
        return this._jogadores.filter( jogador => jogador.nome == nomeJogador  );
    }

    buscarJogadoresPorNumero = ( numeroJogador ) => { 
        return this._jogadores.filter( jogador => jogador.numero == numeroJogador );
    }

    adicionarPartida = ( partida ) => {
        this._partidas.push(partida);
    }

    historicoPartidas = () => {
        return this._partidas;
    }

    mudarStatusTime = () => {
        this._status = !this._status;
    }
}

/** --------------------Objetos------------------------- */

//Testes Unitários.
//Jest -> Mais para ReactJs
//Mocha | Chai -> ReactJs e Vanilla
//Jasmine | Phanton -> Angular
//Cypress -> Novo e ainda pouco usado, mas que promete dominar o mercado.

let matadoresCS = new Time("Matadores CS", "CS", "CS GO");
let morredoresCS = new Time("Morredores CS", "CS", "CS GO");
let CSSTeam = new Time("CSS", "CS", "CS GO");

let claudio = new Jogador("Claudio", 1);
let josimar = new Jogador("Josimar", 2);

matadoresCS.adicionarJogadores(claudio);
matadoresCS.adicionarJogadores(josimar);

matadoresCS.adicionarPartida( new Partidas(matadoresCS, morredoresCS, "34x0") );
matadoresCS.adicionarPartida( new Partidas(matadoresCS, CSSTeams, "15x14") );

console.log(matadoresCS);

console.log( matadoresCS.buscarJogadoresPorNome("Claudio") );
console.log( matadoresCS.buscarJogadoresPorNumero(2) );
console.log( matadoresCS.historicoPartidas() );
