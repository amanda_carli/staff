export default class Episodio {
  constructor( titulo, anoEstreia, diretor, distribuidora, elenco, genero, numeroEpisodios, temporadas ) {
    this.titulo = titulo;
    this.anoEstreia = anoEstreia;
    this.diretor = diretor;
    this.distribuidora = distribuidora;
    this.elenco = elenco;
    this.genero = genero;
    this.numeroEpisodios = numeroEpisodios;
    this.temporadas = temporadas;    
  }

  
}