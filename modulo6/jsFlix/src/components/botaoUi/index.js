import React from 'react';

const BotaoUi = ({classe, metodo, nome}) => <button className={`btn ${ classe }`} onClick={ metodo } >{ nome }</button>

export default BotaoUi;