import java.util.*;

public class EstrategiaIntercalada implements EstrategiaDeAtaque{
    
    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos){
        //Somente elfos vivos
        //50% / 50%
        //Ataques intercalados
        
        if( !validarTodosVivos( elfos ) &&  !validarProporcoes(elfos)) {
            return null;
        }
        
        ArrayList<Elfo> ordem = new ArrayList<>();
        Elfo primeiroElfo = elfos.get(0);
        ordem.add(primeiroElfo);
        Class classeDoUltimoElfo = primeiroElfo.getClass();
        elfos.remove(primeiroElfo);
        
        while( elfos.size() > 0 ) {
            for ( int i = 0; i < elfos.size(); i++) {
                Elfo elfoAtual = elfos.get(i);
                boolean estaIntercalado = elfoAtual.getClass() != classeDoUltimoElfo;
                
                if (estaIntercalado) {
                    ordem.add(elfoAtual);
                    classeDoUltimoElfo = elfoAtual.getClass();
                    elfos.remove(elfoAtual);
                }
            }
        }
        
        return ordem;
    }
    
    private boolean validarTodosVivos( ArrayList<Elfo> elfos ) {
        int contadorMortos = 0;
        
        for (Elfo elfo : elfos) {
            if (elfo.getStatus() == Status.MORTO) {
                contadorMortos++;
            }
        }
        return contadorMortos == 0;
    }
    
    private boolean validarProporcoes( ArrayList<Elfo> elfos ) {
        int contadorVerdes = 0, contadorNoturnos = 0;
        
        for (Elfo elfo : elfos) {
            if (elfo instanceof ElfoVerde) {
                contadorVerdes++;
            }else if(elfo instanceof ElfoNoturno) {
                contadorNoturnos++;
            }
        }
        return contadorVerdes == contadorNoturnos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        return ordenacao(atacantes);
    };
}
