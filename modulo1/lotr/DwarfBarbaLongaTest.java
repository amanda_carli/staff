import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    @Test
    public void dwarfDevePerderVida66porcento() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(4);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.sofrerDano();
        assertEquals(100.0, balin.getVida(), 1e-1);
    }
    
    @Test
    public void dwarfNaoDevePerderVida33porcento() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(5);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.sofrerDano();
        assertEquals(110.0, balin.getVida(), 1e-1);
    }

}