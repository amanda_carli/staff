public class Dwarf extends Personagem {
    private boolean equipado;
    
    {
        this.equipado = false;
        this.qtdDano = 10.0;
    }
    
    public Dwarf( String nome ) {
        super(nome);
        this.vida = 110.0;      
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    protected double calcularDano() {
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public void equiparEscudo() {
        this.equipado = true;
    }
    
    public String imprimirNomeDaClasse() {
        return "Dwarf"; 
    };
    
    /*public void mudarEscudo() {
        this.equipado = !this.equipado;
    }*/
    
    
    
    
  
    
      
}