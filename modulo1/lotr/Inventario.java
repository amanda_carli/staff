import java.util.*;


//public final class Inventario extends Object {
public class Inventario extends Object {
    private ArrayList<Item> itens;
    
    public Inventario( int quantidadeItens ) {
        this.itens = new ArrayList<>(quantidadeItens);
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    public Item obter(int posicao) {
        if( posicao >= this.itens.size() ) {
            return null;
        }
        return this.itens.get(posicao);
    }
    
    public void remover(int posicao) {
        this.itens.remove(posicao);
    }
    
    public void remover(Item item) {
       this.itens.remove(item);
    }
    
    public void adicionar(Item item) {
        this.itens.add(item);
    }
    
    public Item buscar(String descricao) {
        for ( Item item : this.itens ) {
            if ( item.getDescricao().equals(descricao) ) {
                return item;
            }
        }
        return null;
    }
    
    public String getDescricoes() {
        StringBuilder descricoes = new StringBuilder();
        /*for( Item item : this.itens ){
            descricoes.append(item.getDescricao());
            descricoes.append(",");
        }
        
        return descricoes.toString().substring(0, (descricoes.length() - 1));
        */
        
        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            if (item != null) {
                String descricao = item.getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = i < this.itens.size() - 1;
                if( deveColocarVirgula ) {
                    descricoes.append(",");
                }
            }
        }
        
        return descricoes.toString();
    }
    
    public Item getItemComMaiorQuantidade() {
        int indice = 0, maiorQuantidadeParcial = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            if( this.itens.get(i) != null ) {
                //int qtdAtual = this.obter(i).getQuantidade();
                int qtdAtual = this.itens.get(i).getQuantidade();
                if (qtdAtual > maiorQuantidadeParcial) {
                    maiorQuantidadeParcial = qtdAtual;
                    indice = i;
                }
            }
        }
        
        //Logica de comparacao ? se verdadeiro : se falso;
        return this.itens.size() > 0 ? this.obter(indice) : null;
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
        
        for ( int i = this.itens.size() - 1; i >= 0; i-- ){
            listaInvertida.add( this.itens.get(i) );
        }
        
        return listaInvertida;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        for ( int i = 0; i < this.itens.size(); i++) {
            for ( int j = 0; j < this.itens.size() - 1; j++ ) {
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                    atual.getQuantidade() > proximo.getQuantidade() :
                    atual.getQuantidade() < proximo.getQuantidade();
                if( deveTrocar ){
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    }
    
    
    
    
}